import time
from datetime import datetime
from datetime import timedelta

current_time_miliseconds = lambda: int(round(time.time() * 1000))

def datetime_to_miliseconds(dt):
    offset = datetime.utcfromtimestamp(0)
    delta = dt - offset
    return delta.total_seconds() * 1000


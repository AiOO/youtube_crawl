from contextlib import contextmanager
from sqlalchemy.orm import sessionmaker
from secret import engine    

Session = sessionmaker(bind=engine)

@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

import os.path
from sqlalchemy import create_engine

# This is sample db connection.
path = os.path.dirname(os.path.abspath(__file__))
engine = create_engine('sqlite:///%s/secret.db' % path, encoding='utf-8', echo=False)

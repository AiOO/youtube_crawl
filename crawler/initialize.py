from db import session_scope
from db import engine
from model import *

if __name__ == '__main__':
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    default_bot = Bot(name="default", interval=5, status="stop")

    with session_scope() as session:
        session.add(default_bot)

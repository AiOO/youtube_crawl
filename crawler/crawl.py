from bot import get_bots
from datetime import datetime
from datetime import timedelta
from db import session_scope
from threading import Thread
from threading import Timer
from youtube import load_entries_by_author

def crawl(bot):
    bot.status = 'crawling'
    with session_scope() as session:
        session.merge(bot)
    for author in bot.authors:
        last_entry_video_id = None
        escape_flag = True
        page = 0
        while escape_flag:
            page = page + 1
            entry_list = load_entries_by_author(author, page=page)
            with session_scope() as session:
                for entry in entry_list:
                    if entry.video_id == author.last_entry_video_id:
                        escape_flag = False
                        break
                    session.add(entry)
                    if last_entry_video_id is None:
                        last_entry_video_id = entry.video_id
        if last_entry_video_id is not None:
            author.last_entry_video_id = last_entry_video_id
            with session_scope() as session:
                session.merge(author)
    bot.set_timestamp()
    if bot.stop_flag:
        bot.status = 'stop'
    else:
        bot.status = 'wait'
    with session_scope() as session:
        session.merge(bot)

def crawl_all():
    thread_list = []
    for bot in get_bots():
        if bot.status == 'stop' or bot.status == 'crawling':
            continue
        if bot.status == 'wait':
            if bot.last_crawl + timedelta(minutes=bot.interval) <= datetime.now():
                print 'The bot %s has started crawling.' % bot.name
                thread = Thread(target=crawl, args=(bot, ))
                thread.start()
                thread_list.append(thread)
    for thread in thread_list:
        thread.join()

class CrawlThread():
    def __crawl__(self):
        print 'Running crawling cycle...'
        crawl_all()
        self.__timer__ = Timer(60.0, self.__crawl__)
        self.__timer__.start()

    def start(self):
        print 'Startling crawl thread...'
        self.__timer__ = Timer(60.0, self.__crawl__)
        self.__timer__.start()

    def stop(self):
        print 'Stopping crawl thread...'
        self.__timer__.cancel()


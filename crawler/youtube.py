import json
import urllib2
from model import *

url_prefix = 'http://gdata.youtube.com/feeds/api/'

def load_entries_by_author(author, page=1, max_count=10):
    url = url_prefix + \
          'users/%s/uploads?v=2&alt=jsonc&orderby=published&max-results=%s&start-index=%s' % \
          (author.user_id, max_count, max_count * (page - 1) + 1)
    raw_entry_list = json.load(urllib2.urlopen(url))['data']['items']
    entry_list = []
    try:
        for raw_entry in raw_entry_list:
            entry_list.append(Entry.from_dict(raw_entry))
    except:
        return None
    return entry_list

def load_entry(video_id):
    url = url_prefix + "videos/%s?alt=jsonc&v=2" % video_id
    try:
        entry = Entry.from_dict(json.load(urllib2.urlopen(url))['data'])
    except:
        return None
    return entry

def load_author(uploader):
    url = url_prefix + "users/%s?v=2&alt=json" % uploader
    try:
        author = Author.from_dict(json.load(urllib2.urlopen(url))['entry'])
    except:
        return None
    return author

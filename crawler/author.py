from bot import get_default_bot
from db import session_scope
from flask import Blueprint
from flask import render_template
from flask import redirect
from flask import request
from flask import url_for
from model import *
from sqlalchemy.orm import joinedload
from youtube import load_author
from youtube import load_entries_by_author

author = Blueprint('author', __name__)

def get_author_by_id(author_id):
    author = None
    with session_scope() as db_session:
        author = db_session.query(Author).filter(Author.id==author_id).first()
        db_session.expunge_all()
    return author

def get_authors():
    author_list = None
    with session_scope() as db_session:
        author_list = db_session.query(Author).options(joinedload('bot')).all()
        db_session.expunge_all()
    return author_list

def add_author(author):
    # TODO: bot will be set manually
    author.bot = get_default_bot()
    author.last_entry_video_id = load_entries_by_author(author, max_count=1)[0].video_id
    with session_scope() as db_session:
        db_session.add(author)

def remove_author(author):
    with session_scope() as db_session:
        db_session.delete(author)

@author.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if 'command' in request.form:
            command = request.form['command']
            if command == 'remove':
                author = get_author_by_id(request.form['value'])
                if author is not None:
                    remove_author(author)
            elif command == 'add':
                author = load_author(request.form['value'])
                if author is not None:
                    add_author(author)
        return redirect(url_for('author.index'))
    return render_template('author.html', menu='author', author_list=get_authors())


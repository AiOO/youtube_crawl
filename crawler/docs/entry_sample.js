{
 "apiVersion": "2.1",
 "data": {
  "id": "17BeQrjmaA4",
  "uploaded": "2013-07-20T14:00:31.000Z",
  "updated": "2014-02-10T04:21:12.000Z",
  "uploader": "disneyshorts",
  "category": "Film",
  "title": "Mickey Mouse in Stayin' Cool",
  "description": "Mickey, Donald, and Goofy struggle to stay cool on the hottest day of the year.\r\n\r\nSUBSCRIBE and be the first to watch new Disney shorts weekly! \r\n\r\nCheck out more Disney Shorts:http://youtube.com/user/disneyshorts",
  "thumbnail": {
   "sqDefault": "https://i1.ytimg.com/vi/17BeQrjmaA4/default.jpg",
   "hqDefault": "https://i1.ytimg.com/vi/17BeQrjmaA4/hqdefault.jpg"
  },
  "player": {
   "default": "https://www.youtube.com/watch?v=17BeQrjmaA4&feature=youtube_gdata_player",
   "mobile": "https://m.youtube.com/details?v=17BeQrjmaA4"
  },
  "content": {
   "5": "https://www.youtube.com/v/17BeQrjmaA4?version=3&f=videos&app=youtube_gdata"
  },
  "duration": 231,
  "aspectRatio": "widescreen",
  "rating": 4.5071845,
  "likeCount": "3295",
  "ratingCount": 3758,
  "viewCount": 647476,
  "favoriteCount": 0,
  "commentCount": 645,
  "status": {
   "value": "restricted",
   "reason": "requesterRegion"
  },
  "restrictions": [
   {
    "type": "country",
    "relationship": "allow",
    "countries": "US"
   }
  ],
  "accessControl": {
   "comment": "allowed",
   "commentVote": "allowed",
   "videoRespond": "moderated",
   "rate": "allowed",
   "embed": "allowed",
   "list": "allowed",
   "autoPlay": "allowed",
   "syndicate": "allowed"
  }
 }
}

{
 "version": "1.0",
 "encoding": "UTF-8",
 "entry": {
  "xmlns": "http://www.w3.org/2005/Atom",
  "xmlns$media": "http://search.yahoo.com/mrss/",
  "xmlns$gd": "http://schemas.google.com/g/2005",
  "xmlns$yt": "http://gdata.youtube.com/schemas/2007",
  "gd$etag": "W/\"CkMERH47eCp7I2A9Wh9QFE4.\"",
  "id": {
   "$t": "tag:youtube.com,2008:user:K1sVuXaDvJeNwl9noN5nOA"
  },
  "published": {
   "$t": "2011-12-28T02:28:35.000Z"
  },
  "updated": {
   "$t": "2014-02-26T16:00:05.000Z"
  },
  "category": [
   {
    "scheme": "http://schemas.google.com/g/2005#kind",
    "term": "http://gdata.youtube.com/schemas/2007#userProfile"
   }
  ],
  "title": {
   "$t": "KBSEntertain"
  },
  "summary": {
   "$t": "소통과 신뢰를 바탕으로 대한민국 방송을 이끌어 온 KBS는 공영방송만의 고품격 콘텐츠를 전해드리며, 가장 신뢰받는 미디어 1위, 영향력있는 미디어 1위로 선정되었습니다. \n한국인의 중심채널 KBS 는 뉴미디어를 선도하며, 한류를 이끄는 대한민국 콘텐츠의 중심이 되겠습니다."
  },
  "link": [
   {
    "rel": "alternate",
    "type": "text/html",
    "href": "https://www.youtube.com/channel/UCK1sVuXaDvJeNwl9noN5nOA"
   },
   {
    "rel": "self",
    "type": "application/atom+xml",
    "href": "https://gdata.youtube.com/feeds/api/users/K1sVuXaDvJeNwl9noN5nOA?v=2"
   }
  ],
  "author": [
   {
    "name": {
     "$t": "KBSEntertain"
    },
    "uri": {
     "$t": "https://gdata.youtube.com/feeds/api/users/KBSEntertain"
    },
    "yt$userId": {
     "$t": "K1sVuXaDvJeNwl9noN5nOA"
    }
   }
  ],
  "yt$channelId": {
   "$t": "UCK1sVuXaDvJeNwl9noN5nOA"
  },
  "gd$feedLink": [
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.subscriptions",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/subscriptions?v=2",
    "countHint": 3
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.liveevent",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/live/events?v=2",
    "countHint": 0
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.favorites",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/favorites?v=2",
    "countHint": 13
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.contacts",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/contacts?v=2",
    "countHint": 0
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.inbox",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/inbox?v=2"
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.playlists",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/playlists?v=2"
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.uploads",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/uploads?v=2",
    "countHint": 41126
   },
   {
    "rel": "http://gdata.youtube.com/schemas/2007#user.newsubscriptionvideos",
    "href": "https://gdata.youtube.com/feeds/api/users/kbsentertain/newsubscriptionvideos?v=2"
   }
  ],
  "yt$location": {
   "$t": "KR"
  },
  "yt$statistics": {
   "lastWebAccess": "1970-01-01T00:00:00.000Z",
   "subscriberCount": "353667",
   "videoWatchCount": 0,
   "viewCount": "0",
   "totalUploadViews": "255902070"
  },
  "media$thumbnail": {
   "url": "https://i1.ytimg.com/i/K1sVuXaDvJeNwl9noN5nOA/1.jpg?v=509a0400"
  },
  "yt$userId": {
   "$t": "K1sVuXaDvJeNwl9noN5nOA"
  },
  "yt$username": {
   "$t": "kbsentertain",
   "display": "KBSEntertain"
  }
 }
}

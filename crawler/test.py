import json
import urllib2
import pprint
from db import session_scope
from youtube import load_entry
from youtube import load_author
from youtube import load_entries_by_author

pp = pprint.PrettyPrinter(indent=4)

print("Entry test:")
pp.pprint(load_entry('dxNo9H71Zq4').columnitems)
print("")

print("Author test:")
pp.pprint(load_author('KBSEntertain').columnitems)
print("")

print("Entries by author test:")
pp.pprint(load_entries_by_author(load_author('KBSEntertain')))


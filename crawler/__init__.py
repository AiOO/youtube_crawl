import signal
import gevent
from author import author
from bot import bot
from bot import get_bots
from crawl import CrawlThread
from crontab import CronTab
from db import session_scope
from entry import entry
from flask import Flask
from flask import redirect
from flask import render_template
from flask import url_for
from gevent import monkey

monkey.patch_all()

app = Flask(__name__)
app.register_blueprint(entry, url_prefix='/entry')
app.register_blueprint(author, url_prefix='/author')
app.register_blueprint(bot, url_prefix='/bot')
app.teardown_functions = []

def teardown_applications(): 
    for func in app.teardown_functions:
        func()

gevent.signal(signal.SIGINT, teardown_applications)

crawl_thread = CrawlThread()
app.teardown_functions.append(crawl_thread.stop)
app.teardown_functions.append(exit)
crawl_thread.start()

@app.route("/")
def index():
    return redirect(url_for('bot.index'))


from db import session_scope
from flask import Blueprint
from flask import render_template
from model import *

entry = Blueprint('entry', __name__)

def get_entries():
    entry_list = None
    with session_scope() as db_session:
        entry_list = db_session.query(Entry).all()
        db_session.expunge_all()
    return entry_list

@entry.route('/')
def index():
    entry_list = get_entries()
    return render_template('entry.html', menu='entry', entry_list=entry_list)


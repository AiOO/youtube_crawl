from db import session_scope
from flask import Blueprint
from flask import render_template
from flask import request
from model import *
from sqlalchemy.orm import joinedload

bot = Blueprint('bot', __name__)

def get_default_bot():
    bot = None
    with session_scope() as db_session:
        bot = db_session.query(Bot).filter(Bot.id==1).first()
        db_session.expunge_all()
    return bot

def get_bot_by_id(bot_id):
    bot = None
    with session_scope() as db_session:
        bot = db_session.query(Bot).filter(Bot.id==bot_id).first()
        db_session.expunge_all()
    return bot

def get_bots():
    bot_list = None
    with session_scope() as db_session:
        bot_list = db_session.query(Bot).options(joinedload('authors')).all()
        db_session.expunge_all()
    return bot_list

@bot.route('/', methods=["GET", "POST"])
def index():
    if request.method == 'POST':
        print request.form
        if 'command' in request.form:
            command = request.form['command']
            if command == 'wait' or command == 'stop':
                bot = get_bot_by_id(request.form['value'])
                if bot is not None:
                    bot.status = command
                    with session_scope() as session:
                        session.add(bot)
    return render_template('bot.html', \
                           menu='bot',
                           bot_list=get_bots())


from base import Base
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Float
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import Table
from sqlalchemy import Unicode
from sqlalchemy import UnicodeText
from sqlalchemy.orm import backref
from sqlalchemy.orm import relationship
from functions import convert_time

class Entry(Base):
    __tablename__ = "entries"
    __mapping__ = {
        "id": "video_id",
        "thumbnail": {
            "__preset__": "thumbnail",
            "sqDefault": "sq",
            "hqDefault": "hq"
        },
        "player": {
            "__preset__": "player"
        },
        "uploaded": convert_time,
        "updated": convert_time,
        "content": None,
        "aspectRatio": "aspect_ratio",
        "likeCount": "like_count",
        "ratingCount": "rating_count",
        "viewCount": "view_count",
        "favoriteCount": "favorite_count",
        "commentCount": "comment_count",
        "restrictions": {
            "__preset__": "restriction",
            "__list_mode__": "first"
        },
        "accessControl": {
            "__preset__": "access",
            "commentVote": "comment_vote",
            "videoRespond": "video_respond",
            "autoPlay": "auto_play"
        }
    }
    id = Column(Integer, primary_key=True)
    video_id = Column(Unicode, unique=True)
    uploaded = Column(DateTime)
    updated = Column(DateTime)
    uploader = Column(Unicode)
    category = Column(Unicode)
    title = Column(Unicode)
    description = Column(UnicodeText)
    thumbnail_sq = Column(Unicode)
    thumbnail_hq = Column(Unicode)
    player_default = Column(Unicode)
    player_mobile = Column(Unicode)
    duration = Column(Integer)
    aspect_ratio = Column(Unicode)
    rating = Column(Float)
    like_count = Column(Integer)
    rating_count = Column(Integer)
    view_count = Column(Integer)
    favorite_count = Column(Integer)
    comment_count = Column(Integer)
    restriction_type = Column(Unicode)
    restriction_relationship = Column(Unicode)
    restriction_countries = Column(Unicode)
    access_comment = Column(Unicode)
    access_comment_vote = Column(Unicode)
    access_video_respond = Column(Unicode)
    access_rate = Column(Unicode)
    access_embed = Column(Unicode)
    access_list = Column(Unicode)
    access_auto_play = Column(Unicode)
    access_syndicate = Column(Unicode)
    author_id = Column(Integer, ForeignKey('authors.id'))


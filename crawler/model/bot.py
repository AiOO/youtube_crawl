from base import Base
from datetime import datetime
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import Table
from sqlalchemy import Unicode
from sqlalchemy.orm import relationship
from sqlalchemy.orm import backref

class Bot(Base):
    def set_timestamp(self):
        self.last_crawl = datetime.now()

    __tablename__ = 'bots'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode)
    interval = Column(Integer)
    last_crawl = Column(DateTime)
    status = Column(Unicode)
    authors = relationship('Author', backref='bot')
    stop_flag = False


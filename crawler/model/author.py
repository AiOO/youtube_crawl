from base import Base
from functions import convert_time
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Unicode
from sqlalchemy import UnicodeText
from sqlalchemy import Integer
from sqlalchemy import Table
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship

class Author(Base):
    __tablename__ = "authors"
    __mapping__ = {
        "id": None,
        "published": {
            "$t": ("published", convert_time)
        },
        "updated": {
            "$t": ("updated", convert_time)
        },
        "title": {
            "$t": "user_name"
        },
        "summary": {
            "$t": "summary"
        },
        "link": {
            "__list_mode__": "first",
            "href": "link"
        },
        "yt$userId": {
            "$t": "user_id"
        },
        "media$thumbnail": {
            "url": "thumbnail"
        }
    }
    id = Column(Integer, primary_key=True)
    published = Column(DateTime)
    updated = Column(DateTime)
    summary = Column(UnicodeText)
    link = Column(Unicode)
    user_id = Column(Unicode)
    user_name = Column(Unicode)
    thumbnail = Column(Unicode)
    last_entry_video_id = Column(Unicode) 
    entries = relationship('Entry', backref='author')
    bot_id = Column(Integer, ForeignKey('bots.id'))


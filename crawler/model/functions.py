from datetime import datetime
from datetime import timedelta

def convert_time(time_string):
    # convert the time formatted by RFC3339 to datetime(and add 9 hours for KST)
    return datetime.strptime(time_string, '%Y-%m-%dT%H:%M:%S.%fZ') + timedelta(hours=9)

